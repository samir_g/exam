var exams = [];

let index;

$(document).ready(function () {
  $("#form").submit(function (e) {
    e.preventDefault();
    const value = $("input").val();
    $(".exam__nav__body").html(sdutends(exams, value));
  });

  let subjects = [];
  let allPrice = [];

  $("#form-subjects").submit(function (e) {
    e.preventDefault;
    const price = $('input[type="number"]').val();
    const subjectId = $("select").val();
    const subjectName = $("select option:selected").text();
    const student = $('input[type="hidden"]').val();
    // console.log(studen);

    const resultSubject = subjects.filter(function (subject) {
      if (subject.student === student) {
        if (parseInt(subject.id) === parseInt(subjectId)) {
          return subject;
        }
      }
    });

    if (resultSubject.length === 0) {
      //   min = Math.min(...allPrice);
      //   max = Math.max(...allPrice);
      //   avg = (max + min) / 2;
      allPrice.push(Number(price));

      subjects.push({
        id: subjects.length + 1,
        name: subjectName,
        price: price,
        student: student,
      });

      exams[index - 1].props = {
        min: Math.min(...allPrice),
        max: Math.max(...allPrice),
        avg: (Math.min(...allPrice) + Math.max(...allPrice)) / 2,
      };

      const value = $("input").val();

      $(".exam__nav__body").html(sdutends(exams, value));
    }

    let subjectHtml = ``;
    subjects.map(function (subject) {
      if (subject.student == student) {
        subjectHtml += `<tr>
                                        <td>${subject.id}</td>
                                        <td>${subject.name}</td>
                                        <td>${subject.price}</td>
                                    </tr>`;
      }
    });
    $(".subject-detail").html(subjectHtml);
  });

  function sdutends(parametrs, name) {
    const filtered = parametrs.filter((e) => e.name == name);

    if (parametrs.length === 0) {
      if (filtered.length === 0) {
        exams.push({
          id: exams.length + 1,
          name: name,
        });
      }
    } else {
      if (filtered.length === 0) {
        exams.push({
          id: exams.length + 1,
          name: name,
        });
      }
    }

    if (parametrs.length > 0) {
      var html = ``;
      parametrs.map(function (parametr) {
        html += `<tr class="exam__nav__body__result-list">
                <td>${parametr.id}</td>
                <td>${parametr.name}</td>
                <td>
                    <a type="button" data-student-id="${
                      parametr.id
                    }" class="btn btn-primary exam__link" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        Imtahan
                    </a>
                </td>
                <td>${
                  parametr.props !== undefined ? parametr.props.max : ""
                }</td>
                <td>${
                  parametr.props !== undefined ? parametr.props.min : ""
                }</td>
                  <td>${
                    parametr.props !== undefined ? parametr.props.avg : ""
                  }</td>
            </tr>`;
      });

      return html;
    }
  }

  $(document).on("click", ".exam__link", function () {
    $('input[type="hidden"]').val($(this).data("student-id"));
    index = $(this).data("student-id");
    allPrice = [];
    subjects = [];

    let subjectHtml = ``;
    subjects.map(function (subject) {
      if (subject.student == student) {
        subjectHtml += `<tr>
                                        <td>${subject.id}</td>
                                        <td>${subject.name}</td>
                                        <td>${subject.price}</td>
                                    </tr>`;
      }
    });
    $(".subject-detail").html(subjectHtml);
    document.querySelector("select").selectedIndex = 0;
  });
});
